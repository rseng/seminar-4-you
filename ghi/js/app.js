function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${starts} - ${ends}
        </div>
    </div>
    `;
}

function errorMessage(message) {
    const alertTag = document.querySelector('.alert-tag');
    alertTag.innerHTML = `
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Oh naur!</strong> ${message}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    `;
}

function errorToast(message) {
    const toastDiv = document.querySelector('.toast-container');
    toastDiv.innerHTML = `
            <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <i class="bi bi-exclamation-triangle-fill text-danger me-1"></i>
                    <strong class="me-auto">Error</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    ${message}
                </div>
            </div>
    `;
    const toastLive = document.getElementById('liveToast');
    const toast = new bootstrap.Toast(toastLive);
    toast.show();
}

function createPlaceholder() {
    return `
    <div class="card placeholder-card shadow mb-4" aria-hidden="true">
        <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg"
            role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false">
            <title>Placeholder</title>
            <rect width="100%" height="100%" fill="#868e96"></rect>
        </svg>
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    let colIdx = 0;

    try {
        const response = await fetch(url);

        if (!response.ok) {
            errorMessage('Response is not ok');
            errorToast('Response is not ok');
            throw new Error('Response not ok');

        } else {
            const data = await response.json();

            for (let num of data.conferences) {
                const col = columns[colIdx % 3];
                col.innerHTML += createPlaceholder();
                colIdx = (colIdx + 1) % 3;
            }

            colIdx = 0;

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const placeholder = document.querySelector('.placeholder-card')
                    placeholder.remove();
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    const ends = new Date(details.conference.ends);
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts.toLocaleDateString(), ends.toLocaleDateString(), location);
                    const col = columns[colIdx % 3];
                    col.innerHTML += html;
                    colIdx = (colIdx + 1) % 3;
                }
            }

        }
    } catch (e) {
        errorMessage(`${e}`);
        errorToast(`${e}`);
    }

});
